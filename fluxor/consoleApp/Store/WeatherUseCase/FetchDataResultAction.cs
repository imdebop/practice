using System.Collections.Generic;
using consoleApp.Store.Shared;

namespace consoleApp.Store.WeatherUseCase{
    public class FetchDataResultAction{
        public IEnumerable<WeatherForecast> Forecasts {get;}
        public FetchDataResultAction(IEnumerable<WeatherForecast> forecasts){
            Forecasts = forecasts;
        }

    }


}