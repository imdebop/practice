using System.Collections.Generic;
using consoleApp.Store.Shared;
using Fluxor;

namespace consoleApp.Store.WeatherUseCase{
    public class WeatherState{
        public bool IsLoading{get;}
        public IEnumerable<WeatherForecast> Forecasts{get;}

        public WeatherState(bool isLoading, IEnumerable<WeatherForecast> forecasts){
            IsLoading = isLoading;
            Forecasts = forecasts;
        }
    }

    public class Feature:  Feature<WeatherState>{

        public override string GetName() => "Weather";
        protected override WeatherState GetInitialState() =>
            new WeatherState(isLoading: false, forecasts: null);
    }




}