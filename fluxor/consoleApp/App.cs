using System;
using System.Linq;
using Fluxor;
using consoleApp.Store.Shared;
using consoleApp.Store.CounterUseCase;
using consoleApp.Store.WeatherUseCase;

namespace consoleApp
{
    public class App
    {
        private IStore Store;
        public readonly IState<CounterState> CounterState;
        private readonly IState<WeatherState> WeatherState;
        public readonly IDispatcher Dispatcher;

        public App(IStore store, IDispatcher dispatcher, IState<CounterState> counterState, IState<WeatherState> weatherstate){
            Store = store;
            Dispatcher = dispatcher;
            CounterState = counterState;
            CounterState.StateChanged += CounterState_StateChanged;
            WeatherState = weatherstate;
            WeatherState.StateChanged += WeatherState_StateChanged;
        }
        public void Run(){
            Console.Clear();
            Console.WriteLine("Initializing Store.");
            Store.InitializeAsync().Wait();
            string input = "";
            do
            {
                Console.WriteLine("1: Increment counter");
                Console.WriteLine("2: Weather Forecasts");
                Console.WriteLine("x: Exit");
                Console.Write("> ");
                input = Console.ReadLine();

                switch(input.ToLowerInvariant())
                {
                    case "1":
                    var action = new IncrementCounterAction();
                    Dispatcher.Dispatch(action);
                    break;

                    case "2":
                    var fetchDataAction = new FetchDataAction();
                    Dispatcher.Dispatch(fetchDataAction);
                    break;

                    case "x":
                    Console.WriteLine("Program terminated");
                    return;
                }

            } while (true);
        }

        private void CounterState_StateChanged(object sender, CounterState e){
            Console.WriteLine("");
            Console.WriteLine("==========================> CounterState");
            Console.WriteLine("ClickCount is " + CounterState.Value.ClickCount);
            Console.WriteLine("<========================== CounterState");
            Console.WriteLine("");
        }

        private void WeatherState_StateChanged(object sender, WeatherState e)
        {
            Console.WriteLine("");
            Console.WriteLine("=========================> WeatherState");
            Console.WriteLine("IsLoading: " + WeatherState.Value.IsLoading);
            if (!WeatherState.Value.Forecasts.Any())
            {
                Console.WriteLine("--- No weather forecasts");
            }
            else
            {
                Console.WriteLine("Temp C\tTemp F\tSummary");
                foreach (WeatherForecast forecast in WeatherState.Value.Forecasts)
                Console.WriteLine($"{forecast.TemperatureC}\t{forecast.TemperatureF}\t{forecast.Summary}");
            }
            Console.WriteLine("<========================== WeatherState");
            Console.WriteLine("");
        }
    }
}