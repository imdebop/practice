using System.Web;
using Microsoft.AspNetCore.Mvc;
using poormanDI.Services;

namespace poormanDI.Controllers{
    public class DemoController: ControllerBase{
        private IDemo _demoService = null;
        private IDemo _serviceTest = new DemoServices();
        //public DemoController(): this(new DemoServices()){ }
        public DemoController(IDemo service = null){
            if(service is null){
                _demoService = _serviceTest;
            }
        }

        [HttpGet]
        [Route("api/display")]
        public string DisplayMethod(){
            string result = _demoService.Display();
            return result;
        }

    }



}