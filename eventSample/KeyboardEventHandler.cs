using System;
using System.Threading;
using System.Threading.Tasks;


namespace eventSample
{
    public delegate void KeyboardEventHandler(char eventCode);
    public class KeyboardEventLoop{
        public event KeyboardEventHandler _onKeyDown;

        public KeyboardEventLoop(KeyboardEventHandler onKeyDown){
            _onKeyDown = onKeyDown;
        }        

        public Task Start(CancellationToken ct){
            return Task.Run(()=> EventLoop(ct));
        }

        void EventLoop(CancellationToken ct){
            while(!ct.IsCancellationRequested){
                string line = Console.ReadLine();
                char eventCode = (line == null || line.Length == 0) ? '\0' : line[0];
                _onKeyDown(eventCode);
            }
        }
    }
}