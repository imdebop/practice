using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;

namespace simpleOne.Store{
    public class AppState{
        public string SelectedColor {get; private set;}

        public event Action OnChange;

        public void SetColor(string color){
            SelectedColor = color;
            NotifyStateChanged();
        }

        private void NotifyStateChanged() => OnChange.Invoke();
    }


}